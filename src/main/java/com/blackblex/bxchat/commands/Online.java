/**
 * BXChat Server
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package commands
 *
 * ==============Information==============
 *      Filename: Online.java
 * ---------------------------------------
*/

package com.blackblex.bxchat.commands;

import java.io.IOException;
import java.util.Map;

import com.blackblex.bxchat.app.Startup;
import com.blackblex.bxchat.frames.ServerFrame;
import com.blackblex.libs.core.interfaces.custom.CommandInterface;
import com.blackblex.libs.net.objects.SocketMessage;
import com.blackblex.libs.net.objects.SocketUsername;

public class Online implements CommandInterface {

	@Override
	public String getDescription() {
		return "Realiza el registro del usuario como tambien notifica a los demas usuarios";
	}

	@Override
	public boolean canExecute(SocketUsername socketUsername) {
		return true;
	}

	@Override
	public void execute(SocketUsername socketUsername, SocketMessage dataInput) {
		SocketUsername user = Startup.clientsOnline.get(dataInput.getFrom());
		user.setUsername(dataInput.getMessage());
		SocketMessage dataOutput = new SocketMessage();
		if (Startup.clientsOnline.size() <= Startup.maxClients) {
			if (Startup.commandList.containsKey("send")) {
				CommandInterface command = Startup.commandList.get("send");
				if (command.canExecute(socketUsername)) {
					dataOutput.setAction("add");
					dataOutput.setFrom(dataInput.getFrom());
					dataOutput.setTo(-1);
					dataOutput.setMessage(dataInput.getMessage());
					command.execute(socketUsername, dataOutput);
					for (Map.Entry<Integer, SocketUsername> client : Startup.clientsOnline.entrySet()) {
						if (client.getKey() != dataInput.getFrom()) {
							dataOutput.setFrom(client.getValue().getIdSession());
							dataOutput.setMessage(client.getValue().getUsername());
							user.sendClient(dataOutput);
						}
					}
				}
			}
		} else {
			dataOutput.setAction("full");
			dataOutput.setFrom(-1);
			dataOutput.setTo(-1);
			dataOutput.setMessage("");
			socketUsername.sendClient(dataOutput);
			ServerFrame.jtextAreaConsole.append(Startup.clientsOnline.get(socketUsername.getIdSession())
					+ " se ha desconectado -> Servidor lleno.\n");
			try {
				socketUsername.getSocketClient().close();
			} catch (IOException e) {

			}
			Startup.clientsOnline.remove(dataInput.getFrom());
		}
	}
}
