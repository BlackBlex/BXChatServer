/**
 * BXChat Server
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package frames
 *
 * ==============Information==============
 *      Filename: Frame.java
 * ---------------------------------------
*/

package com.blackblex.bxchat.frames;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.blackblex.libs.core.interfaces.jcomponent.FrameInterface;

public abstract class Frame extends JFrame implements FrameInterface {
	private static final long serialVersionUID = 1101321772870126818L;
	public JPanel jpanelHeader, jpanelBody, jpanelBottom;
	public Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	public int FRAME_WIDTH = (int) ((screenSize.width) * 0.50);
	public int FRAME_HEIGHT = (int) ((screenSize.height) * 0.50);
	public final GridBagConstraints gridbag = new GridBagConstraints();

	public Frame() {
		beforeInit();
		init();
		afterInit();
	}

	public void beforeInit() {
		jpanelHeader = new JPanel();
		jpanelBody = new JPanel();
		jpanelBottom = new JPanel();
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		jpanelHeader.setLayout(new BoxLayout(jpanelHeader, BoxLayout.X_AXIS));
		jpanelHeader.setBorder(new EmptyBorder(10, 10, 10, 10));

		jpanelBody.setLayout(new GridBagLayout());
		jpanelBody.setBorder(new EmptyBorder(10, 10, 10, 10));

		jpanelBottom.setLayout(new GridBagLayout());
		jpanelBottom.setBorder(new EmptyBorder(0, 10, 10, 10));
	}

	public void init() {
		this.add(jpanelHeader);
		this.add(jpanelBody);
		this.add(jpanelBottom);
	}

	public void showFrame(String nameApp, String title) {
		if (!title.isEmpty())
			this.setTitle(nameApp + " ~ " + title);

		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public void showFrame(String nameApp) {
		if (!nameApp.isEmpty())
			this.setTitle(nameApp);

		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

}
