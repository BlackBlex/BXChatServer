/**
 * BXChat Server
 *
 * @author  Jovani Perez Damian (@BlackBlex)
 * @license General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package app
 *
 * ==============Information==============
 *      Filename: Main.java
 * ---------------------------------------
*/

package com.blackblex.bxchat;

import com.blackblex.bxchat.app.Startup;

public class App {
	public static void main(String args[]) {
		new Startup();
	}
}
