**BXChat Server**
===================


Es un chat escrito en java que contiene un sistema de plugins que complementerá el sistema.


*Requiere:* [BlexLib](https://gitlab.com/BlackBlex/blexlib)

*Importante:* La rama (main) contiene codigo solo para ejecutar en Java 9 en adelante, si quiere usar Java 8, use la rama Java 8

--------
**Caracteristicas:**

 - Enviar mensajes a todos los usuarios conectados.
 - Sistema de plugins.

--------
**Plugins disponibles:**

 - **SendPMMessage** ~ Para que identifique el servidor cuando se trata de un mensaje privado | Complemento de SendPMMessage - [BXChat Client](https://gitlab.com/BlackBlex/BXChatClient)
 - **SendFile** ~ Para que identifique el servidor cuando se trata de un envió de archivo | Complemento de SendFile - [BXChat Client](https://gitlab.com/BlackBlex/BXChatClient)

--------

**Nota:** *Se irá añadiendo más características conforme se vaya avanzando en su desarrollo. **No cuenta con sistema de seguridad, se implementará más adelante***

--------

Para ejecutar se debe:
1. Clonar el repositorio
2. Abrir con VSCode y tener instalado [Java pack Extension](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack)
3. Abrir el archivo Main.java ubicado en **src/app**
4. Ejecutar el archivo desde VSCode

--------

Imagen:

Principal

![](images/main.png)

Conexiones

![](images/conexiones.png)

Servidor lleno

![](images/servidor_lleno.png)

Charla

![](images/charla.png)


--------
 BXChat | Chat básico con sistema de plugins

 Servidor escrito en java

 Author: @BlackBlex (BlackBlex)

 License: General Public License (GPLv3) | http://www.gnu.org/licenses/
